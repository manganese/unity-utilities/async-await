# Manganese - Unity Utilities - Async/Await

A wrapper around Modest Tree Media's AsyncAwaitUtil library for inclusion in Unity.

## Installation

Add the Manganese Package Registry as a scoped registry to your Unity project's `manifest.json` file:

```
  "scopedRegistries": [
    {
      "name": "Manganese",
      "url": "http://unity.packages.mangane.se",
      "scopes": [
        "com.manganese"
      ]
    }
  ]
```

The Unity Package Manager should now find this package and give you the option to install it.

## Usage

See the official [GitHub repository](https://github.com/modesttree/Unity3dAsyncAwaitUtil) for more details.